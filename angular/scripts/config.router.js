/**
* @ngdoc function
* @name app.config:uiRouter
* @description
* # Config
* Config for the router
*/
(function() {
    'use strict';
    angular
    .module('app')
    .run(runBlock)
    .config(config);

    runBlock.$inject = ['$rootScope', '$state', '$stateParams'];
    function runBlock($rootScope,   $state,   $stateParams) {
        $rootScope.$state = $state;
        $rootScope.$stateParams = $stateParams;        
    }

    config.$inject =  ['$stateProvider', '$urlRouterProvider', 'MODULE_CONFIG'];
    function config( $stateProvider,   $urlRouterProvider,   MODULE_CONFIG ) {

        var p = getParams('layout'),
        l = p ? p + '.' : '',
        layout = '../views/layout/layout.'+l+'html',
        dashboard = '../views/home/dashboard.'+l+'html';

        $urlRouterProvider
        .otherwise('/app/home/dashboard');
        $stateProvider
        .state('app', {
            abstract: true,
            url: '/app',
            views: {
                '': {
                    templateUrl: layout
                }
            }
        })


        // 처음화면
        .state('app.home', {
            url: '/home',
            template: '<div ui-view></div>'
        })
        .state('app.home.dashboard', {
            url: '/dashboard',
            templateUrl: '../views/home/dashboard.html',
            data : {title: '처음화면'},
            controller: "DashboardCtrl",
            resolve: load(['scripts/controllers/home/dashboard.js','scripts/controllers/home/notice.js','scripts/controllers/home/update.js','scripts/controllers/home/promotion.js','scripts/controllers/home/manual.js'])
        })
        .state('app.home.notice', {
            url: '/notice',
            templateUrl: '../views/home/notice.html',
            data : {title: '처음화면'},
            controller: "NoticeCtrl",
            resolve: load(['scripts/controllers/home/notice.js'])
        })
        .state('app.home.promotion', {
            url: '/promotion',
            templateUrl: '../views/home/promotion.html',
            data : {title: '처음화면'},
            controller: "PromoCtrl",
            resolve: load(['scripts/controllers/home/promotion.js'])
        })
        .state('app.home.update', {
            url: '/update',
            templateUrl: '../views/home/update.html',
            data : {title: '처음화면'},
            controller: "UpdateCtrl",
            resolve: load(['scripts/controllers/home/update.js'])
        })
        .state('app.home.manual', {
            url: '/manual',
            templateUrl: '../views/home/manual.html',
            data : {title: '처음화면'},
            controller: "ManualCtrl",
            resolve: load(['scripts/controllers/home/manual.js'])
        })
        .state('app.home.view', {
            url: '/view',
            templateUrl: '../views/home/view.html',
            data : {title: '처음화면'},
            controller: "NoticeCtrl",
            resolve: load(['scripts/controllers/home/notice.js'])
        })

        // 가맹점설정
        .state('app.store', {
            url: '/store',
            template: '<div ui-view></div>'
        })
        .state('app.store.info', {
            url: '/info',
            templateUrl: '../views/store/info.html',
            data : { title: '가맹점 정보 설정' },
            controller: 'InfoCtrl',
            resolve: load(['scripts/controllers/store/info.js','ngImgCrop','scripts/controllers/imgcrop.js'])
        })
        .state('app.store.install', {
            url: '/install',
            templateUrl: '../views/store/install.html',
            data : { title: '가맹점 정보 설정' },
            controller: 'InstallCtrl',
            resolve: load(['scripts/controllers/store/install.js'])
        })
        .state('app.store.pgsetting', {
            url: '/pgsetting',
            templateUrl: '../views/store/pgsetting.html',
            data : { title: '가맹점 정보 설정' },
            controller: 'PGsettingCtrl',
            resolve: load(['scripts/controllers/store/pgsetting.js'])
        })
        .state('app.store.pgsetting2', {
            url: '/pgsetting2',
            templateUrl: '../views/store/pgsetting2.html',
            data : { title: '가맹점 정보 설정' },
            controller: 'PGsetting2Ctrl',
            resolve: load(['scripts/controllers/store/pgsetting2.js'])
        })
        .state('app.store.creditcard', {
            url: '/creditcard',
            templateUrl: '../views/store/creditcard.html',
            data : { title: '가맹점 정보 설정' },
            controller: "CreditCardCtrl",
            resolve: load(['scripts/controllers/store/creditcard.js','mgcrea.ngStrap','scripts/controllers/angularstrap.js'])
        })

        // 데이터베이스
        .state('app.database', {
            url: '/database',
            template: '<div ui-view></div>'
        })
        .state('app.database.records', {
            url: '/records',
            templateUrl: '../views/database/records.html',
            data : { title: '결제 승인 내역' },
            controller: "recordsCtrl",
            resolve: load(['scripts/controllers/database/records.js'])
        })

        // 통계분석 - 서비스현황
        .state('app.revenue', {
            url: '/revenue',
            template: '<div ui-view></div>'
        })
        .state('app.revenue.summary', {
            url: '/summary',
            templateUrl: '../views/revenue/summary.html',
            data : { title: '서비스 현황'},
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })
        .state('app.revenue.date', {
            url: '/date',
            templateUrl: '../views/revenue/date.html',
            data : { title: '서비스 현황'},
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })
        .state('app.revenue.time', {
            url: '/time',
            templateUrl: '../views/revenue/time.html',
            data : { title: '서비스 현황' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })
        .state('app.revenue.location', {
            url: '/location',
            templateUrl: '../views/revenue/location.html',
            data : { title: '서비스 현황' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })
        .state('app.revenue.device', {
            url: '/device',
            templateUrl: '../views/revenue/device.html',
            data : { title: '서비스 현황' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })
        .state('app.revenue.group', {
            url: '/group',
            templateUrl: '../views/revenue/group.html',
            data : { title: '서비스 현황' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })

        // 통계분석 - 결제환경 진단
        .state('app.pay', {
            url: '/pay',
            template: '<div ui-view></div>'
        })
        .state('app.pay.summary', {
            url: '/summary',
            templateUrl: '../views/pay/summary.html',
            data : { title: '결제상태 분석' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })
        .state('app.pay.try', {
            url: '/try',
            templateUrl: '../views/pay/try.html',
            data : { title: '결제상태 분석' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })
        .state('app.pay.paid', {
            url: '/paid',
            templateUrl: '../views/pay/paid.html',
            data : { title: '결제상태 분석' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })
        .state('app.pay.failed', {
            url: '/failed',
            templateUrl: '../views/pay/failed.html',
            data : { title: '결제상태 분석' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })
        .state('app.pay.canceled', {
            url: '/canceled',
            templateUrl: '../views/pay/canceled.html',
            data : { title: '결제상태 분석' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js','scripts/controllers/color.js'])
        })

        // 통계분석 - 이용자 분석
        .state('app.pattern', {
            url: '/pattern',
            template: '<div ui-view></div>'
        })
        .state('app.pattern.summary', {
            url: '/paymethod',
            templateUrl: '../views/pattern/summary.html',
            data : { title: '결제수단 분석' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js'])
        })
        .state('app.pattern.specific', {
            url: '/mspecific',
            templateUrl: '../views/pattern/specific.html',
            data : { title: '카드/은행별 상세보기' },
            controller: "ChartCtrl",
            resolve: load(['scripts/controllers/chart.js'])
        })

        // 실시간
        .state('app.live', {
            url: '/live',
            template: '<div ui-view></div>'
        })
        .state('app.live.monitor', {
            url: '/monitor',
            templateUrl: '../views/live/monitor.html',
            data : { title: '실시간 현황' },
            controller: "MonitorCtrl",
            resolve: load(['scripts/controllers/live/monitor.js'])
        })


        // 서비스 이용현황
        .state('app.store.now', {
            url: '/now',
            templateUrl: '../views/store/now.html',
            data : { title: '가맹점 정보설정' }
        })
        .state('app.store.plan', {
            url: '/plan',
            templateUrl: '../views/store/plan.html',
            data : { title: '가맹점 정보설정' },
            controller: 'PlanCtrl',
            resolve: load(['scripts/controllers/store/plan.js'])
        })
        .state('app.store.wallet', {
            url: '/wallet',
            templateUrl: '../views/store/wallet.html',
            data : { title: '가맹점 정보설정' },
            controller: 'WalletCtrl',
            resolve: load(['scripts/controllers/store/wallet.js'])
        })
        .state('app.store.history', {
            url: '/history',
            templateUrl: '../views/store/history.html',
            data : { title: '가맹점 정보설정' }
        })

        // 회원가입/로그인/비밀번호찾기
        .state('access', {
            url: '/access',
            template: '<div class="dark bg-auto w-full"><div ui-view class="fade-in smooth pos-rlt"></div></div>'
        })
        .state('access.signin', {
            url: '/signin',
            templateUrl: '../views/misc/signin.html'
        })
        .state('access.signup', {
            url: '/signup',
            templateUrl: '../views/misc/signup.html'
        })
        .state('access.forgot-password', {
            url: '/forgot-password',
            templateUrl: '../views/misc/forgot-password.html'
        })
        .state('404', {
            url: '/404',
            templateUrl: '../views/misc/404.html'
        })
        .state('505', {
            url: '/505',
            templateUrl: '../views/misc/505.html'
        })
        .state('access.lockme', {
            url: '/lockme',
            templateUrl: '../views/misc/lockme.html'
        })

        // 문제해결
        .state('app.help', {
            url: '/help',
            template: '<div ui-view></div>'
        })
        .state('app.help.faq', {
            url: '/faq',
            templateUrl: '../views/help/faq.html',
            data : { title: '자주 하는 질문' },
            controller: "FAQCtrl",
            resolve: load(['scripts/controllers/help/faq.js'])
        })
        .state('app.help.cscenter', {
            url: '/cscenter',
            templateUrl: '../views/help/cscenter.html',
            data : { title: '고객센터', hideFooter: true }
        })


        ;

        function load(srcs, callback) {
            return {
                deps: ['$ocLazyLoad', '$q', 
                function( $ocLazyLoad, $q ){
                    var deferred = $q.defer();
                    var promise  = false;
                    srcs = angular.isArray(srcs) ? srcs : srcs.split(/\s+/);
                    if(!promise){
                        promise = deferred.promise;
                    }
                    angular.forEach(srcs, function(src) {
                        promise = promise.then( function(){
                            angular.forEach(MODULE_CONFIG, function(module) {
                                if( module.name == src){
                                    src = module.module ? module.name : module.files;
                                }
                            });
                            return $ocLazyLoad.load(src);
                        } );
                    });
                    deferred.resolve();
                    return callback ? promise.then(function(){ return callback(); }) : promise;
                }]
            }
        }

        function getParams(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }
    }
})();
