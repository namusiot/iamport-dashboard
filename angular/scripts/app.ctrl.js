/**
* @ngdoc function
* @name app.controller:AppCtrl
* @description
* # MainCtrl
* Controller of the app
*/

(function() {
    'use strict';
    angular
    .module('app')
    .controller('AppCtrl', AppCtrl);

    AppCtrl.$inject  = ['$scope', '$localStorage', '$location', '$rootScope', '$anchorScroll', '$timeout', '$window'];

    function AppCtrl($scope, $localStorage, $location, $rootScope, $anchorScroll, $timeout, $window) {
        var vm = $scope;
        vm.isIE = isIE();
        vm.isSmart = isSmart();
            // config
            vm.app = {
                name: 'Iamport',
                version: '1.0',
            // for chart colors
            color: {
                'primary':      '#0cc2aa',
                'accent':       '#a88add',
                'warn':         '#fcc100',
                'info':         '#6887ff',
                'success':      '#6cc788',
                'warning':      '#f77a99',
                'danger':       '#f44455',
                'white':        '#ffffff',
                'light':        '#f1f2f3',
                'dark':         '#2e3e4e',
                'black':        '#2a2b3c'
            },
            setting: {
                theme: {
                    primary: 'primary',
                    accent: 'accent',
                    warn: 'warn'
                },
                folded: false,
                boxed: false,
                container: false,
                themeID: 1,
                bg: ''
            },
            store: {
                name: '셀잇',
                url: 'www.withsellit.com',
                image: '../assets/images/logo-sellit-2.png',
                impuid: 'imp13233401',
                admin: 'admin@withsellit.com',
                about: '판매도 구매도 끝까지 책임지는 중고거래, 셀잇! 모바일 중고거래, 셀잇이 모든 것을 책임집니다. 사기 0%, 판매자도 구매자도 믿을 수 있는 모바일 중고장터. 셀잇 포터가 직접 픽업합니다. (서울지역) 안쓰는 물건은 간편하게 모바일로 팔고, 필요한 물건은 안심하고 쇼핑하세요. 중고나라, 번개장터, 헬로마켓과 같은 개인간 거래와는 비교할 수 없는 안전함과 편리함을 누리세요.',
                restapikey: '3017995188913544',
                restapisecret: 'FEsn7jhrltbgpJrdyrpVYRo5fEoD7uON9gagArHWVTckCExb6X64Ova3azHi4v6eLgumIciD5vXI9vTf',
                membership: 'paid',
                membership_date: '2016-03-15'
            },
            nav: [
                {
                    menu: '처음화면',
                    href: '#/app/home/dashboard',
                    sref: 'app.home.dashboard',
                    page: 'app.home',
                    icon: 'icon-dashboard.png',
                    lock: false
                },
                {
                    menu: '가맹점 정보설정',
                    href: '#/app/store/info',
                    sref: 'app.store.info',
                    page: 'app.store',
                    icon: 'icon-setting.png',
                    lock: false
                },
                {
                    menu: '결제 승인 내역',
                    href: '#/app/database/records',
                    sref: 'app.database.records',
                    page: 'app.database',
                    icon: 'icon-database.png',
                    lock: false
                },
                {
                    menu: '서비스 현황',
                    href: '#/app/revenue/summary',
                    sref: 'app.revenue.summary',
                    page: 'app.revenue',
                    icon: 'icon-service.png',
                    lock: true
                },
                {
                    menu: '결제상태 분석',
                    href: '#/app/pay/summary',
                    sref: 'app.pay.summary',
                    page: 'app.pay',
                    icon: 'icon-device.png',
                    lock: true
                },
                {
                    menu: '결제수단 분석',
                    href: '#/app/pattern/summary',
                    sref: 'app.pattern.summary',
                    page: 'app.pattern',
                    icon: 'icon-user.png',
                    lock: true
                },
                {
                    menu: '실시간 모니터링',
                    href: '#/app/live/monitor',
                    sref: 'app.live.monitor',
                    page: 'app.live',
                    icon: 'icon-live.png',
                    lock: true
                }
            ]
        };

        var setting = vm.app.name+'-Setting';
            // save settings to local storage
            if ( angular.isDefined($localStorage[setting]) ) {
                vm.app.setting = $localStorage[setting];
            } else {
                $localStorage[setting] = vm.app.setting;
        }
        // watch changes
        $scope.$watch('app.setting', function(){
            $localStorage[setting] = vm.app.setting;
        }, true);

        getParams('bg') && (vm.app.setting.bg = getParams('bg'));

        vm.setTheme = setTheme;
        setColor();

        function setTheme(theme){
            vm.app.setting.theme = theme.theme;
            setColor();
            if(theme.url){
                $timeout(function() {
                    $window.location.href = theme.url;
                }, 100, false);
            }
        };

        function setColor(){
            vm.app.setting.color = {
                primary: getColor(vm.app.setting.theme.primary),
                accent: getColor(vm.app.setting.theme.accent),
                warn: getColor(vm.app.setting.theme.warn)
            };
        };

        function getColor(name){
            return vm.app.color[ name ] ? vm.app.color[ name ] : palette.find(name);
        };

        $rootScope.$on('$stateChangeSuccess', openPage);

        function openPage() {
            // goto top
            $location.hash('content');
            $anchorScroll();
            $location.hash('');
            // hide open menu
            $('#aside').modal('hide');
            $('body').removeClass('modal-open').find('.modal-backdrop').remove();
            $('.navbar-toggleable-sm').collapse('hide');
        };

        vm.goBack = function () {
            $window.history.back();
        };

        function isIE() {
            return !!navigator.userAgent.match(/MSIE/i) || !!navigator.userAgent.match(/Trident.*rv:11\./);
        }

        function isSmart(){
        // Adapted from http://www.detectmobilebrowsers.com
        var ua = $window['navigator']['userAgent'] || $window['navigator']['vendor'] || $window['opera'];
        // Checks for iOs, Android, Blackberry, Opera Mini, and Windows mobile devices
        return (/iPhone|iPod|iPad|Silk|Android|BlackBerry|Opera Mini|IEMobile/).test(ua);
        }

        function getParams(name) {
            name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
            var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
            results = regex.exec(location.search);
            return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
        }

    }
})();
