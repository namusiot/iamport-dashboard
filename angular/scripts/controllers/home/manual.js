(function() {
    'use strict';
    angular
    .module('app')
    .controller('ManualCtrl', manualsetting);

    function manualsetting($scope, $interval){
        var vm = $scope;

        vm.manualList = [
            {
                title:'나에게 맞는 PG 가입하기',
                content:'아임포트의 결제모듈을 사용하게되면 다음과 같은 장점이 있습니다. javascript를 사용해 결제모듈 연동이 이루어지므로 다양한 개발 환경(Python, Ruby, Node.js 등)에 쉽게 적용될 수 있습니다. 결제정보 조회 / 결제 취소 / 매출전표 출력 등 운영에 필요한 관리자 기능을 기본제공하고 있어 편리합니다. 이용 중이던 PG사를 변경할 때, 관리자 페이지에서 PG정보만 변경하면 소스코드 수정이 필요없어 빠릅니다. PC용 / mobile용 구분해 작업할 필요없이 동일한 javascript 코드로 연동이 가능합니다.(mobile 추가옵션 확인) UTF-8 charset으로 동작하므로 euc-kr변환작업이 필요없습니다.'
            },
            {
                title:'아임포트 결제모듈 구조',
                content:'아임포트의 결제모듈을 사용하게되면 다음과 같은 장점이 있습니다. javascript를 사용해 결제모듈 연동이 이루어지므로 다양한 개발 환경(Python, Ruby, Node.js 등)에 쉽게 적용될 수 있습니다. 결제정보 조회 / 결제 취소 / 매출전표 출력 등 운영에 필요한 관리자 기능을 기본제공하고 있어 편리합니다. 이용 중이던 PG사를 변경할 때, 관리자 페이지에서 PG정보만 변경하면 소스코드 수정이 필요없어 빠릅니다. PC용 / mobile용 구분해 작업할 필요없이 동일한 javascript 코드로 연동이 가능합니다.(mobile 추가옵션 확인) UTF-8 charset으로 동작하므로 euc-kr변환작업이 필요없습니다.'
            },
            {
                title:'Notification URL 설정하기',
                content:'아임포트의 결제모듈을 사용하게되면 다음과 같은 장점이 있습니다. javascript를 사용해 결제모듈 연동이 이루어지므로 다양한 개발 환경(Python, Ruby, Node.js 등)에 쉽게 적용될 수 있습니다. 결제정보 조회 / 결제 취소 / 매출전표 출력 등 운영에 필요한 관리자 기능을 기본제공하고 있어 편리합니다. 이용 중이던 PG사를 변경할 때, 관리자 페이지에서 PG정보만 변경하면 소스코드 수정이 필요없어 빠릅니다. PC용 / mobile용 구분해 작업할 필요없이 동일한 javascript 코드로 연동이 가능합니다.(mobile 추가옵션 확인) UTF-8 charset으로 동작하므로 euc-kr변환작업이 필요없습니다.'
            },
            {
                title:'PG정보 설정하기',
                content:'아임포트의 결제모듈을 사용하게되면 다음과 같은 장점이 있습니다. javascript를 사용해 결제모듈 연동이 이루어지므로 다양한 개발 환경(Python, Ruby, Node.js 등)에 쉽게 적용될 수 있습니다. 결제정보 조회 / 결제 취소 / 매출전표 출력 등 운영에 필요한 관리자 기능을 기본제공하고 있어 편리합니다. 이용 중이던 PG사를 변경할 때, 관리자 페이지에서 PG정보만 변경하면 소스코드 수정이 필요없어 빠릅니다. PC용 / mobile용 구분해 작업할 필요없이 동일한 javascript 코드로 연동이 가능합니다.(mobile 추가옵션 확인) UTF-8 charset으로 동작하므로 euc-kr변환작업이 필요없습니다.'
            },
            {
                title:'복수 PG 사용하기',
                content:'아임포트의 결제모듈을 사용하게되면 다음과 같은 장점이 있습니다. javascript를 사용해 결제모듈 연동이 이루어지므로 다양한 개발 환경(Python, Ruby, Node.js 등)에 쉽게 적용될 수 있습니다. 결제정보 조회 / 결제 취소 / 매출전표 출력 등 운영에 필요한 관리자 기능을 기본제공하고 있어 편리합니다. 이용 중이던 PG사를 변경할 때, 관리자 페이지에서 PG정보만 변경하면 소스코드 수정이 필요없어 빠릅니다. PC용 / mobile용 구분해 작업할 필요없이 동일한 javascript 코드로 연동이 가능합니다.(mobile 추가옵션 확인) UTF-8 charset으로 동작하므로 euc-kr변환작업이 필요없습니다.'
            },
            {
                title:'통계분석 사용하기',
                content:'아임포트의 결제모듈을 사용하게되면 다음과 같은 장점이 있습니다. javascript를 사용해 결제모듈 연동이 이루어지므로 다양한 개발 환경(Python, Ruby, Node.js 등)에 쉽게 적용될 수 있습니다. 결제정보 조회 / 결제 취소 / 매출전표 출력 등 운영에 필요한 관리자 기능을 기본제공하고 있어 편리합니다. 이용 중이던 PG사를 변경할 때, 관리자 페이지에서 PG정보만 변경하면 소스코드 수정이 필요없어 빠릅니다. PC용 / mobile용 구분해 작업할 필요없이 동일한 javascript 코드로 연동이 가능합니다.(mobile 추가옵션 확인) UTF-8 charset으로 동작하므로 euc-kr변환작업이 필요없습니다.'
            },
            {
                title:'실시간 모니터링 사용하기',
                content:'아임포트의 결제모듈을 사용하게되면 다음과 같은 장점이 있습니다. javascript를 사용해 결제모듈 연동이 이루어지므로 다양한 개발 환경(Python, Ruby, Node.js 등)에 쉽게 적용될 수 있습니다. 결제정보 조회 / 결제 취소 / 매출전표 출력 등 운영에 필요한 관리자 기능을 기본제공하고 있어 편리합니다. 이용 중이던 PG사를 변경할 때, 관리자 페이지에서 PG정보만 변경하면 소스코드 수정이 필요없어 빠릅니다. PC용 / mobile용 구분해 작업할 필요없이 동일한 javascript 코드로 연동이 가능합니다.(mobile 추가옵션 확인) UTF-8 charset으로 동작하므로 euc-kr변환작업이 필요없습니다.'
            },
            {
                title:'API Call Limit 해제하기',
                content:'아임포트의 결제모듈을 사용하게되면 다음과 같은 장점이 있습니다. javascript를 사용해 결제모듈 연동이 이루어지므로 다양한 개발 환경(Python, Ruby, Node.js 등)에 쉽게 적용될 수 있습니다. 결제정보 조회 / 결제 취소 / 매출전표 출력 등 운영에 필요한 관리자 기능을 기본제공하고 있어 편리합니다. 이용 중이던 PG사를 변경할 때, 관리자 페이지에서 PG정보만 변경하면 소스코드 수정이 필요없어 빠릅니다. PC용 / mobile용 구분해 작업할 필요없이 동일한 javascript 코드로 연동이 가능합니다.(mobile 추가옵션 확인) UTF-8 charset으로 동작하므로 euc-kr변환작업이 필요없습니다.'
            }
        ]
        
        vm.showtile = function(){
            $("#tile").removeClass("dp-none");
            $("#list").addClass("dp-none");

        }
        vm.showlist = function(){
            $("#list").removeClass("dp-none");
            $("#tile").addClass("dp-none");

        }
        
    };

})();




