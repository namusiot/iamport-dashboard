// code style: https://github.com/johnpapa/angular-styleguide 

(function() {
    'use strict';
    angular
    .module('app')
    .controller('PromoCtrl', promosetting);

    function promosetting($scope, $interval){
        var vm = $scope;

        // 프로모션 목록
        vm.promotionList = [
            {name:"KG이니시스",memo1:"연관리비",memo2:"(20만원)",memo3:"평생면제",icon:"kg2",link:""},
            {name:"나이스정보통신",memo1:"연관리비",memo2:"(10만원)",memo3:"평생면제",icon:"nice",link:""},
            {name:"JTNET",memo1:"연관리비",memo2:"(20만원)",memo3:"평생면제",icon:"jtnet",link:""},
            {name:"카카오페이",memo1:"연관리비",memo2:"(20만원)",memo3:"평생면제",icon:"kakao2",link:""},
            {name:"다날 (휴대폰결제)",memo1:"가입비 및 연관리비",memo2:"",memo3:"면제",icon:"danal",link:""},
        ];
        
        vm.showtile = function(){
            $("#tile").removeClass("dp-none");
            $("#list").addClass("dp-none");

        }
        vm.showlist = function(){
            $("#list").removeClass("dp-none");
            $("#tile").addClass("dp-none");

        }

    };

})();




