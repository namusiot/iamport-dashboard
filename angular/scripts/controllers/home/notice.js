// code style: https://github.com/johnpapa/angular-styleguide 

(function() {
    'use strict';
    angular
    .module('app')
    .controller('NoticeCtrl', noticesetting);

    function noticesetting($scope, $interval){
        var vm = $scope;

        // 공지사항 목록
        vm.noticeList = [
            {
                title:'EasyDigitalDownloads에서도 아임포트를 이용해보세요.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-01.jpg',
                content:'어떠한 개발환경이든 결제연동을 손쉽게 만들어드리는 아임포트 서비스가 Woocommerce에 이어 EasyDigitalDownloads플러그인도 결제연동을 지원합니다. 0.9.0버전으로 시작하지만, Woocommerce 용 플러그인이 향상 되어온 것처럼 다양한 사용자의 의견을 수렴해 꾸준히 버전업데이트할 수 있도록 노력하겠습니다.'
            },{
                title:'아임포트가 우커머스 정기결제와 해외카드결제를 지원합니다.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-02.png',
                content:'정기결제 서비스를 필요로 하시는 경우 Woocommerce Subscription 플러그인을 구매해 우커머스 기능을 확장해 사용하실텐데요, Woocommerce Subscription플러그인과 연동되는 국내카드 결제 기능이 아임포트 플러그인 1.4.1버전부터 제공됩니다. 또한, 해외발급카드 소지자를 대상으로 결제가 필요한 경우 KRW(원화)결제를 받으실 수 있도록 해외카드 결제 옵션도 1.4.1버전에 추가되었습니다. 아임포트 플러그인 1.4.1 버전을 설치 / 업데이트 해보세요!'
            },{
                title:'아임포트가 KG이니시스의 간편결제인 KPay를 지원합니다.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-03.jpg',
                content:'아임포트 우커머스용 결제플러그인을 [1.3.9]버전으로 업데이트하시면 “아임포트(KPay) 결제수단"이 추가됩니다. 구매자는 활성화된 KPay결제수단 옵션을 선택함으로써, 기존처럼 신용카드사 선택단계를 거쳐 KPay를 시작하지않고 곧바로 KPay결제 프로세스를 시작할 수 있습니다. KPay는 KG이니시스 사용자라면 누구나 이용이 가능하며, 카카오페이처럼 카드정보를 “KPay 모바일앱”에 저장해두고 쉽게 결제하는 KG이니시스의 간편결제입니다.'
            },{
                title:'아임포트와 함께 장고로 쇼핑몰 만들기 강좌가 생겼습니다.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-04.png',
                content:'아임포트를 이용하여 손쉽게 쇼핑몰을 구현하는 강의가 등장했습니다! 파이썬 장고(django)를 이용한 강좌로 파이썬 개발자 분들의 많은 관심 바랍니다. 자세히 보기 : https://festi.kr/class/iamport/'
            },{
                title:'아임포트가 휴대폰결제 전문 PG사인 다날을 지원합니다.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-05.jpg',
                content:'판매자들이 보다 유연한 정산주기와 저렴한 수수료로 휴대폰소액결제를 이용할 수 있도록 휴대폰결제 전문 PG사인 다날을 아임포트 우커머스 결제 플러그인에서 공식 지원합니다. 다날에서는 판매자의 필요에 따라 휴대폰결제 정산 주기를 유연하게 설정할 수 있도록 주 단위부터 지원하며(선 정산), 아임포트와 제휴를 통해 보다 저렴한 수수료 (3.2%~)를 제공하고 있습니다. 아래 이미지 순서대로 간단하게 설정을 하면 아임포트 플러그인으로 다날의 휴대폰결제를 이용할 수 있습니다. 아임포트 x 다날과 제휴 프로모션을 통해 어떠한 비용도 지불하지 않고 무료로 계약할 수 있으니 휴대폰 결제 비중이 높은 판매자들은 다날의 휴대폰 결제를 통해 보다 유리한 조건으로 판매를 진행하세요.'
            },{
                title:'EasyDigitalDownloads에서도 아임포트를 이용해보세요.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-01.jpg',
                content:'어떠한 개발환경이든 결제연동을 손쉽게 만들어드리는 아임포트 서비스가 Woocommerce에 이어 EasyDigitalDownloads플러그인도 결제연동을 지원합니다. 0.9.0버전으로 시작하지만, Woocommerce 용 플러그인이 향상 되어온 것처럼 다양한 사용자의 의견을 수렴해 꾸준히 버전업데이트할 수 있도록 노력하겠습니다.'
            },{
                title:'아임포트가 우커머스 정기결제와 해외카드결제를 지원합니다.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-02.png',
                content:'정기결제 서비스를 필요로 하시는 경우 Woocommerce Subscription 플러그인을 구매해 우커머스 기능을 확장해 사용하실텐데요, Woocommerce Subscription플러그인과 연동되는 국내카드 결제 기능이 아임포트 플러그인 1.4.1버전부터 제공됩니다. 또한, 해외발급카드 소지자를 대상으로 결제가 필요한 경우 KRW(원화)결제를 받으실 수 있도록 해외카드 결제 옵션도 1.4.1버전에 추가되었습니다. 아임포트 플러그인 1.4.1 버전을 설치 / 업데이트 해보세요!'
            },{
                title:'아임포트가 KG이니시스의 간편결제인 KPay를 지원합니다.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-03.jpg',
                content:'아임포트 우커머스용 결제플러그인을 [1.3.9]버전으로 업데이트하시면 “아임포트(KPay) 결제수단"이 추가됩니다. 구매자는 활성화된 KPay결제수단 옵션을 선택함으로써, 기존처럼 신용카드사 선택단계를 거쳐 KPay를 시작하지않고 곧바로 KPay결제 프로세스를 시작할 수 있습니다. KPay는 KG이니시스 사용자라면 누구나 이용이 가능하며, 카카오페이처럼 카드정보를 “KPay 모바일앱”에 저장해두고 쉽게 결제하는 KG이니시스의 간편결제입니다.'
            },{
                title:'아임포트와 함께 장고로 쇼핑몰 만들기 강좌가 생겼습니다.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-04.png',
                content:'아임포트를 이용하여 손쉽게 쇼핑몰을 구현하는 강의가 등장했습니다! 파이썬 장고(django)를 이용한 강좌로 파이썬 개발자 분들의 많은 관심 바랍니다. 자세히 보기 : https://festi.kr/class/iamport/'
            },{
                title:'아임포트가 휴대폰결제 전문 PG사인 다날을 지원합니다.',
                date:'2016-02-24',
                writer:'아임포트 서비스팀',
                image:'../assets/images/notice/sample-05.jpg',
                content:'판매자들이 보다 유연한 정산주기와 저렴한 수수료로 휴대폰소액결제를 이용할 수 있도록 휴대폰결제 전문 PG사인 다날을 아임포트 우커머스 결제 플러그인에서 공식 지원합니다. 다날에서는 판매자의 필요에 따라 휴대폰결제 정산 주기를 유연하게 설정할 수 있도록 주 단위부터 지원하며(선 정산), 아임포트와 제휴를 통해 보다 저렴한 수수료 (3.2%~)를 제공하고 있습니다. 아래 이미지 순서대로 간단하게 설정을 하면 아임포트 플러그인으로 다날의 휴대폰결제를 이용할 수 있습니다. 아임포트 x 다날과 제휴 프로모션을 통해 어떠한 비용도 지불하지 않고 무료로 계약할 수 있으니 휴대폰 결제 비중이 높은 판매자들은 다날의 휴대폰 결제를 통해 보다 유리한 조건으로 판매를 진행하세요.'
            }
        ]

        vm.showtile = function(){
            $("#tile").removeClass("dp-none");
            $("#list").addClass("dp-none");

        }
        vm.showlist = function(){
            $("#list").removeClass("dp-none");
            $("#tile").addClass("dp-none");

        }

    };

})();




