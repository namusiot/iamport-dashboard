(function() {
    'use strict';
    angular
      .module('app')
      .controller('ChartCtrl', chartsetting);

      function chartsetting($scope){
        var vm = $scope;

        // 랜덤데이터 - 월단위
        var range_month = 31;     // 차트 가로축의 범위
        vm.axis_m = []; for(var i=0;i < range_month;i++) vm.axis_m.push(i+1);
        vm.data_m = []; for(var i=0;i < range_month;i++) vm.data_m.push(Math.floor(Math.random()));
        vm.data_m_1 = []; for(var i=0;i < range_month;i++) vm.data_m_1.push(Math.floor(Math.random()*50+100));
        vm.data_m_2 = []; for(var i=0;i < range_month;i++) vm.data_m_2.push(Math.floor(Math.random()*50+100));
        vm.data_m_3 = []; for(var i=0;i < range_month;i++) vm.data_m_3.push(Math.floor(Math.random()*50+100));
        vm.data_m_4 = []; for(var i=0;i < range_month;i++) vm.data_m_4.push(Math.floor(Math.random()*50+100));
        vm.data_m_5 = []; for(var i=0;i < range_month;i++) vm.data_m_5.push(Math.floor(Math.random()*50+100));
        vm.data_m_6 = []; for(var i=0;i < range_month;i++) vm.data_m_6.push(Math.floor(Math.random()*50+100));
        vm.data_m_7 = []; for(var i=0;i < range_month;i++) vm.data_m_7.push(Math.floor(Math.random()*50+100));
        vm.data_m_8 = []; for(var i=0;i < range_month;i++) vm.data_m_8.push(Math.floor(Math.random()*50+100));
        vm.data_m_9 = []; for(var i=0;i < range_month;i++) vm.data_m_9.push(Math.floor(Math.random()*50+100));

        // Dashboard        
        vm.chart_01 = [[1,25],[2,22],[3,18],[4,24],[5,32],[6,59],[7,83],[8,76],[9,100],[10,87],[11,88],[12,74],[13,79],[14,65],[15,59],[16,58],[17,72],[18,54]];
        vm.chart_02 = [[1,10200],[2,12500],[3,11200],[4,14200],[5,22200],[6,29200],[7,33200],[8,56200],[9,50230],[10,55200],[11,58200],[12,74200],[13,89200],[14,85200],[15,79200],[16,78200],[17,72200],[18,54500]];
        vm.p_l_1 = [[1, 6.1], [2, 6.3], [3, 6.4], [4, 6.6], [5, 7.0], [6, 7.7], [7, 8.3]];
        vm.p_l_2 = [[1, 5.5], [2, 5.7], [3, 6.4], [4, 7.0], [5, 7.2], [6, 7.3], [7, 7.5]];
        vm.p_l_3 = [[1, 2], [2, 1.6], [3, 2.4], [4, 2.1], [5, 1.7], [6, 1.5], [7, 1.7]];
        vm.p_l_4 = [[1, 3], [2, 2.6], [3, 3.2], [4, 3], [5, 3.5], [6, 3], [7, 3.5]];
        vm.p_l_5 = [[1, 3.6], [2, 3.5], [3, 6], [4, 4], [5, 4.3], [6, 3.5], [7, 3.6]];
        vm.p_l_6 = [[1, 10], [2, 8], [3, 27], [4, 25], [5, 50], [6, 30], [7, 25]];

        
        // 차트값레이어(툴팁)수정, 툴박스제거
        vm.tooltip_style = "toolbox: {show : false}, calculable : false, tooltip : {trigger: 'axis', backgroundColor:'rgba(0,0,0,0.8)', borderRadius:3, padding:8, showDelay:0, hideDelay:0, axisPointer : {type: 'shadow'}, textStyle: {fontSize : 12}}";

        // 차트축기준선제거
        vm.axisline_style = "axisLine : {lineStyle: {width:'0'}}, axisTick : {show:false}";
        
    };
})();
