// code style: https://github.com/johnpapa/angular-styleguide 

(function() {
    'use strict';
    angular
    .module('app')
    .controller('MonitorCtrl', monitorsetting);

    function monitorsetting($scope, $interval){
        var vm = $scope;

        vm.live_count = 0;
        vm.live_value = 0;
        vm.live_data = [];

        vm.string = function(){
            // var text = "[" + vm.live_count + "," + vm.live_value + "]";
            var text = vm.live_value;
            return text;
        }

        $interval(function pushnumber(){
            vm.live_count += 1;
            vm.live_value = Math.floor(Math.random()*10);
            vm.live_data.push(vm.string());
            vm.live_data_string = JSON.stringify(vm.live_data);
            vm.live_data_string = vm.live_data_string.replace(/"/gi,'')
        }, 1000);
        
        

    }
})();
