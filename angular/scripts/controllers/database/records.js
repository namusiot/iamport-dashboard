// code style: https://github.com/johnpapa/angular-styleguide 

(function() {
      'use strict';
      angular
      .module('app')
      .controller('recordsCtrl', databasesetting);

      function databasesetting($scope, $interval){
            
            var vm = $scope;

            $interval(function(){}, 1000); // 동적인 숫자카운트 효과

            // 기본
            vm.impuid_active = 99;


        // 랜덤데이터 - 월단위
        var range_month = 31;     // 차트 가로축의 범위
        vm.axis_m = []; for(var i=0;i < range_month;i++) vm.axis_m.push(i+1);
        vm.data_m = []; for(var i=0;i < range_month;i++) vm.data_m.push(Math.floor(Math.random()));
        vm.data_m_1 = []; for(var i=0;i < range_month;i++) vm.data_m_1.push(Math.floor(Math.random()*50+100));
        vm.data_m_2 = []; for(var i=0;i < range_month;i++) vm.data_m_2.push(Math.floor(Math.random()*50+100));
        vm.data_m_3 = []; for(var i=0;i < range_month;i++) vm.data_m_3.push(Math.floor(Math.random()*50+100));
        vm.data_m_4 = []; for(var i=0;i < range_month;i++) vm.data_m_4.push(Math.floor(Math.random()*50+100));
        vm.data_m_5 = []; for(var i=0;i < range_month;i++) vm.data_m_5.push(Math.floor(Math.random()*50+100));
        vm.data_m_6 = []; for(var i=0;i < range_month;i++) vm.data_m_6.push(Math.floor(Math.random()*50+100));
        vm.data_m_7 = []; for(var i=0;i < range_month;i++) vm.data_m_7.push(Math.floor(Math.random()*50+100));
        vm.data_m_8 = []; for(var i=0;i < range_month;i++) vm.data_m_8.push(Math.floor(Math.random()*50+100));
        vm.data_m_9 = []; for(var i=0;i < range_month;i++) vm.data_m_9.push(Math.floor(Math.random()*50+100));

        // Dashboard        
        vm.chart_01 = [[1,25],[2,22],[3,18],[4,24],[5,32],[6,59],[7,83],[8,76],[9,100],[10,87],[11,88],[12,74],[13,79],[14,65],[15,59],[16,58],[17,72],[18,54]];
        vm.chart_02 = [[1,10200],[2,12500],[3,11200],[4,14200],[5,22200],[6,29200],[7,33200],[8,56200],[9,50230],[10,55200],[11,58200],[12,74200],[13,89200],[14,85200],[15,79200],[16,78200],[17,72200],[18,54500]];
        vm.p_l_1 = [[1, 6.1], [2, 6.3], [3, 6.4], [4, 6.6], [5, 7.0], [6, 7.7], [7, 8.3]];
        vm.p_l_2 = [[1, 5.5], [2, 5.7], [3, 6.4], [4, 7.0], [5, 7.2], [6, 7.3], [7, 7.5]];
        vm.p_l_3 = [[1, 2], [2, 1.6], [3, 2.4], [4, 2.1], [5, 1.7], [6, 1.5], [7, 1.7]];
        vm.p_l_4 = [[1, 3], [2, 2.6], [3, 3.2], [4, 3], [5, 3.5], [6, 3], [7, 3.5]];
        vm.p_l_5 = [[1, 3.6], [2, 3.5], [3, 6], [4, 4], [5, 4.3], [6, 3.5], [7, 3.6]];
        vm.p_l_6 = [[1, 10], [2, 8], [3, 27], [4, 25], [5, 50], [6, 30], [7, 25]];

        
        // 차트값레이어(툴팁)수정, 툴박스제거
        vm.tooltip_style = "toolbox: {show : false}, calculable : false, tooltip : {trigger: 'axis', backgroundColor:'rgba(0,0,0,0.8)', borderRadius:3, padding:8, showDelay:0, hideDelay:0, axisPointer : {type: 'shadow'}, textStyle: {fontSize : 12}}";

        // 차트축기준선제거
        vm.axisline_style = "axisLine : {lineStyle: {width:'0'}}, axisTick : {show:false}";
        
        // 컬러셋
        vm.red = "#f44336";
        vm.red50 = "#ffebee";
        vm.red100 = "#ffcdd2";
        vm.red200 = "#ef9a9a";
        vm.red300 = "#e57373";
        vm.red400 = "#ef5350";
        vm.red500 = "#f44336";
        vm.red600 = "#e53935";
        vm.red700 = "#d32f2f";
        vm.red800 = "#c62828";
        vm.red900 = "#b71c1c";
        vm.redA100 = "#ff8a80";
        vm.redA200 = "#ff5252";
        vm.redA400 = "#ff1744";
        vm.redA700 = "#d50000";
        vm.pink = "#e91e63";
        vm.pink50 = "#fce4ec";
        vm.pink100 = "#f8bbd0";
        vm.pink200 = "#f48fb1";
        vm.pink300 = "#f06292";
        vm.pink400 = "#ec407a";
        vm.pink500 = "#e91e63";
        vm.pink600 = "#d81b60";
        vm.pink700 = "#c2185b";
        vm.pink800 = "#ad1457";
        vm.pink900 = "#880e4f";
        vm.pinkA100 = "#ff80ab";
        vm.pinkA200 = "#ff4081";
        vm.pinkA400 = "#f50057";
        vm.pinkA700 = "#c51162";
        vm.purple = "#9c27b0";
        vm.purple50 = "#f3e5f5";
        vm.purple100 = "#e1bee7";
        vm.purple200 = "#ce93d8";
        vm.purple300 = "#ba68c8";
        vm.purple400 = "#ab47bc";
        vm.purple500 = "#9c27b0";
        vm.purple600 = "#8e24aa";
        vm.purple700 = "#7b1fa2";
        vm.purple800 = "#6a1b9a";
        vm.purple900 = "#4a148c";
        vm.purpleA100 = "#ea80fc";
        vm.purpleA200 = "#e040fb";
        vm.purpleA400 = "#d500f9";
        vm.purpleA700 = "#aa00ff";
        vm.deeppurple = "#673ab7";
        vm.deeppurple50 = "#ede7f6";
        vm.deeppurple100 = "#d1c4e9";
        vm.deeppurple200 = "#b39ddb";
        vm.deeppurple300 = "#9575cd";
        vm.deeppurple400 = "#7e57c2";
        vm.deeppurple500 = "#673ab7";
        vm.deeppurple600 = "#5e35b1";
        vm.deeppurple700 = "#512da8";
        vm.deeppurple800 = "#4527a0";
        vm.deeppurple900 = "#311b92";
        vm.deeppurpleA100 = "#b388ff";
        vm.deeppurpleA200 = "#7c4dff";
        vm.deeppurpleA400 = "#651fff";
        vm.deeppurpleA700 = "#6200ea";
        vm.indigo = "#3f51b5";
        vm.indigo50 = "#e8eaf6";
        vm.indigo100 = "#c5cae9";
        vm.indigo200 = "#9fa8da";
        vm.indigo300 = "#7986cb";
        vm.indigo400 = "#5c6bc0";
        vm.indigo500 = "#3f51b5";
        vm.indigo600 = "#3949ab";
        vm.indigo700 = "#303f9f";
        vm.indigo800 = "#283593";
        vm.indigo900 = "#1a237e";
        vm.indigoA100 = "#8c9eff";
        vm.indigoA200 = "#536dfe";
        vm.indigoA400 = "#3d5afe";
        vm.indigoA700 = "#304ffe";
        vm.blue = "#2196f3";
        vm.blue50 = "#e3f2fd";
        vm.blue100 = "#bbdefb";
        vm.blue200 = "#90caf9";
        vm.blue300 = "#64b5f6";
        vm.blue400 = "#42a5f5";
        vm.blue500 = "#2196f3";
        vm.blue600 = "#1e88e5";
        vm.blue700 = "#1976d2";
        vm.blue800 = "#1565c0";
        vm.blue900 = "#0d47a1";
        vm.blueA100 = "#82b1ff";
        vm.blueA200 = "#448aff";
        vm.blueA400 = "#2979ff";
        vm.blueA700 = "#2962ff";
        vm.lightblue = "#03a9f4";
        vm.lightblue50 = "#e1f5fe";
        vm.lightblue100 = "#b3e5fc";
        vm.lightblue200 = "#81d4fa";
        vm.lightblue300 = "#4fc3f7";
        vm.lightblue400 = "#29b6f6";
        vm.lightblue500 = "#03a9f4";
        vm.lightblue600 = "#039be5";
        vm.lightblue700 = "#0288d1";
        vm.lightblue800 = "#0277bd";
        vm.lightblue900 = "#01579b";
        vm.lightblueA100 = "#80d8ff";
        vm.lightblueA200 = "#40c4ff";
        vm.lightblueA400 = "#00b0ff";
        vm.lightblueA700 = "#0091ea";
        vm.cyan = "#00bcd4";
        vm.cyan50 = "#e0f7fa";
        vm.cyan100 = "#b2ebf2";
        vm.cyan200 = "#80deea";
        vm.cyan300 = "#4dd0e1";
        vm.cyan400 = "#26c6da";
        vm.cyan500 = "#00bcd4";
        vm.cyan600 = "#00acc1";
        vm.cyan700 = "#0097a7";
        vm.cyan800 = "#00838f";
        vm.cyan900 = "#006064";
        vm.cyanA100 = "#84ffff";
        vm.cyanA200 = "#18ffff";
        vm.cyanA400 = "#00e5ff";
        vm.cyanA700 = "#00b8d4";
        vm.teal = "#009688";
        vm.teal50 = "#e0f2f1";
        vm.teal100 = "#b2dfdb";
        vm.teal200 = "#80cbc4";
        vm.teal300 = "#4db6ac";
        vm.teal400 = "#26a69a";
        vm.teal500 = "#009688";
        vm.teal600 = "#00897b";
        vm.teal700 = "#00796b";
        vm.teal800 = "#00695c";
        vm.teal900 = "#004d40";
        vm.tealA100 = "#a7ffeb";
        vm.tealA200 = "#64ffda";
        vm.tealA400 = "#1de9b6";
        vm.tealA700 = "#00bfa5";
        vm.green = "#4caf50";
        vm.green50 = "#e8f5e9";
        vm.green100 = "#c8e6c9";
        vm.green200 = "#a5d6a7";
        vm.green300 = "#81c784";
        vm.green400 = "#66bb6a";
        vm.green500 = "#4caf50";
        vm.green600 = "#43a047";
        vm.green700 = "#388e3c";
        vm.green800 = "#2e7d32";
        vm.green900 = "#1b5e20";
        vm.greenA100 = "#b9f6ca";
        vm.greenA200 = "#69f0ae";
        vm.greenA400 = "#00e676";
        vm.greenA700 = "#00c853";
        vm.lightgreen = "#8bc34a";
        vm.lightgreen50 = "#f1f8e9";
        vm.lightgreen100 = "#dcedc8";
        vm.lightgreen200 = "#c5e1a5";
        vm.lightgreen300 = "#aed581";
        vm.lightgreen400 = "#9ccc65";
        vm.lightgreen500 = "#8bc34a";
        vm.lightgreen600 = "#7cb342";
        vm.lightgreen700 = "#689f38";
        vm.lightgreen800 = "#558b2f";
        vm.lightgreen900 = "#33691e";
        vm.lightgreenA100 = "#ccff90";
        vm.lightgreenA200 = "#b2ff59";
        vm.lightgreenA400 = "#76ff03";
        vm.lightgreenA700 = "#64dd17";
        vm.lime = "#cddc39";
        vm.lime50 = "#f9fbe7";
        vm.lime100 = "#f0f4c3";
        vm.lime200 = "#e6ee9c";
        vm.lime300 = "#dce775";
        vm.lime400 = "#d4e157";
        vm.lime500 = "#cddc39";
        vm.lime600 = "#c0ca33";
        vm.lime700 = "#afb42b";
        vm.lime800 = "#9e9d24";
        vm.lime900 = "#827717";
        vm.limeA100 = "#f4ff81";
        vm.limeA200 = "#eeff41";
        vm.limeA400 = "#c6ff00";
        vm.limeA700 = "#aeea00";
        vm.yellow = "#ffeb3b";
        vm.yellow50 = "#fffde7";
        vm.yellow100 = "#fff9c4";
        vm.yellow200 = "#fff59d";
        vm.yellow300 = "#fff176";
        vm.yellow400 = "#ffee58";
        vm.yellow500 = "#ffeb3b";
        vm.yellow600 = "#fdd835";
        vm.yellow700 = "#fbc02d";
        vm.yellow800 = "#f9a825";
        vm.yellow900 = "#f57f17";
        vm.yellowA100 = "#ffff8d";
        vm.yellowA200 = "#ffff00";
        vm.yellowA400 = "#ffea00";
        vm.yellowA700 = "#ffd600";
        vm.amber = "#ffc107";
        vm.amber50 = "#fff8e1";
        vm.amber100 = "#ffecb3";
        vm.amber200 = "#ffe082";
        vm.amber300 = "#ffd54f";
        vm.amber400 = "#ffca28";
        vm.amber500 = "#ffc107";
        vm.amber600 = "#ffb300";
        vm.amber700 = "#ffa000";
        vm.amber800 = "#ff8f00";
        vm.amber900 = "#ff6f00";
        vm.amberA100 = "#ffe57f";
        vm.amberA200 = "#ffd740";
        vm.amberA400 = "#ffc400";
        vm.amberA700 = "#ffab00";
        vm.orange = "#ff9800";
        vm.orange50 = "#fff3e0";
        vm.orange100 = "#ffe0b2";
        vm.orange200 = "#ffcc80";
        vm.orange300 = "#ffb74d";
        vm.orange400 = "#ffa726";
        vm.orange500 = "#ff9800";
        vm.orange600 = "#fb8c00";
        vm.orange700 = "#f57c00";
        vm.orange800 = "#ef6c00";
        vm.orange900 = "#e65100";
        vm.orangeA100 = "#ffd180";
        vm.orangeA200 = "#ffab40";
        vm.orangeA400 = "#ff9100";
        vm.orangeA700 = "#ff6d00";
        vm.deeporange = "#ff5722";
        vm.deeporange50 = "#fbe9e7";
        vm.deeporange100 = "#ffccbc";
        vm.deeporange200 = "#ffab91";
        vm.deeporange300 = "#ff8a65";
        vm.deeporange400 = "#ff7043";
        vm.deeporange500 = "#ff5722";
        vm.deeporange600 = "#f4511e";
        vm.deeporange700 = "#e64a19";
        vm.deeporange800 = "#d84315";
        vm.deeporange900 = "#bf360c";
        vm.deeporangeA100 = "#ff9e80";
        vm.deeporangeA200 = "#ff6e40";
        vm.deeporangeA400 = "#ff3d00";
        vm.deeporangeA700 = "#dd2c00";
        vm.brown = "#795548";
        vm.brown50 = "#efebe9";
        vm.brown100 = "#d7ccc8";
        vm.brown200 = "#bcaaa4";
        vm.brown300 = "#a1887f";
        vm.brown400 = "#8d6e63";
        vm.brown500 = "#795548";
        vm.brown600 = "#6d4c41";
        vm.brown700 = "#5d4037";
        vm.brown800 = "#4e342e";
        vm.brown900 = "#3e2723";
        vm.bluegrey = "#607d8b";
        vm.bluegrey50 = "#eceff1";
        vm.bluegrey100 = "#cfd8dc";
        vm.bluegrey200 = "#b0bec5";
        vm.bluegrey300 = "#90a4ae";
        vm.bluegrey400 = "#78909c";
        vm.bluegrey500 = "#607d8b";
        vm.bluegrey600 = "#546e7a";
        vm.bluegrey700 = "#455a64";
        vm.bluegrey800 = "#37474f";
        vm.bluegrey900 = "#263238";
        vm.grey50 = "#fafafa";
        vm.grey100 = "#f5f5f5";
        vm.grey200 = "#eeeeee";
        vm.grey300 = "#e0e0e0";
        vm.grey400 = "#bdbdbd";
        vm.grey500 = "#9e9e9e";
        vm.grey600 = "#757575";
        vm.grey700 = "#616161";
        vm.grey800 = "#424242";
        vm.grey900 = "#212121";
      
      

            // MID List
            vm.midList = [
                  {
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:15',
                        order_price:14000,
                        order_cancel:0,
                        order_buyer_name:'유재석',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:30',
                        order_price:18400,
                        order_cancel:0,
                        order_buyer_name:'박명수',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:55:59',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정준하',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:50',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정형돈',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'11.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'나이스정보통신',
                        pg_storeid:'NicepayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  }
            ];



            // MID List
            vm.midList2 = [
                  {
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:15',
                        order_price:14000,
                        order_cancel:0,
                        order_buyer_name:'유재석',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:30',
                        order_price:18400,
                        order_cancel:0,
                        order_buyer_name:'박명수',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:55:59',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정준하',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:50',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정형돈',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'11.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'나이스정보통신',
                        pg_storeid:'NicepayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:15',
                        order_price:14000,
                        order_cancel:0,
                        order_buyer_name:'유재석',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:30',
                        order_price:18400,
                        order_cancel:0,
                        order_buyer_name:'박명수',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:55:59',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정준하',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:50',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정형돈',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'11.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'나이스정보통신',
                        pg_storeid:'NicepayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:15',
                        order_price:14000,
                        order_cancel:0,
                        order_buyer_name:'유재석',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:30',
                        order_price:18400,
                        order_cancel:0,
                        order_buyer_name:'박명수',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:55:59',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정준하',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:50',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정형돈',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'11.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'나이스정보통신',
                        pg_storeid:'NicepayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:15',
                        order_price:14000,
                        order_cancel:0,
                        order_buyer_name:'유재석',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:30',
                        order_price:18400,
                        order_cancel:0,
                        order_buyer_name:'박명수',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:55:59',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정준하',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:50',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정형돈',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'11.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'나이스정보통신',
                        pg_storeid:'NicepayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:15',
                        order_price:14000,
                        order_cancel:0,
                        order_buyer_name:'유재석',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:30',
                        order_price:18400,
                        order_cancel:0,
                        order_buyer_name:'박명수',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:55:59',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정준하',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:'결제가 완료되었습니다.',
                        status_dtg:'2016-06-01 08:00:01'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:50',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정형돈',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'11.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'나이스정보통신',
                        pg_storeid:'NicepayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:50',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정형돈',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'11.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'나이스정보통신',
                        pg_storeid:'NicepayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        imp_uid:'imp_636581903311',
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-05-26 13:10:01',
                        order_price:158000,
                        order_cancel:0,
                        order_buyer_name:'김재윤',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'failed',
                        status_msg:'F0004:PG사 결제요청에 실패하여 중단합니다.(imp_637072649538) [코드: V022] 필수 파라미터가 누락되었습니다.[price:Failed to convert property value of type java.lang.String to required type java.lang.Long for property price; nested exception is org.springframework.core.convert.ConversionFa',
                        status_dtg:'2016-05-26 13:13:14'
                  },{
                        imp_uid:'imp_636581903311',
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-05-26 13:10:01',
                        order_price:158000,
                        order_cancel:0,
                        order_buyer_name:'김재윤',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'failed',
                        status_msg:'F0004:PG사 결제요청에 실패하여 중단합니다.(imp_637072649538) [코드: V022] 필수 파라미터가 누락되었습니다.[price:Failed to convert property value of type java.lang.String to required type java.lang.Long for property price; nested exception is org.springframework.core.convert.ConversionFa',
                        status_dtg:'2016-05-26 13:13:14'
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:50',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정형돈',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'11.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'나이스정보통신',
                        pg_storeid:'NicepayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },{
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-06-01 07:54:50',
                        order_price:5850,
                        order_cancel:0,
                        order_buyer_name:'정형돈',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'11.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'나이스정보통신',
                        pg_storeid:'NicepayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  }
            ];


            // 선택한 MID에 해당하는 IMPUID 정보 로드
            vm.orderList = [
                  {
                        imp_uid:'imp_636581903311',
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-05-26 13:10:01',
                        order_price:158000,
                        order_cancel:0,
                        order_buyer_name:'김재윤',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'failed',
                        status_msg:'F0004:PG사 결제요청에 실패하여 중단합니다.(imp_637072649538) [코드: V022] 필수 파라미터가 누락되었습니다.[price:Failed to convert property value of type java.lang.String to required type java.lang.Long for property price; nested exception is org.springframework.core.convert.ConversionFa',
                        status_dtg:'2016-05-26 13:13:14'
                  },
                  {
                        imp_uid:'imp_636581903312',
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-05-26 13:14:14',
                        order_price:158000,
                        order_cancel:0,
                        order_buyer_name:'김재윤',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Macintosh',
                        client_os:'OS X',
                        client_os_v:'10.10.5',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'ready',
                        status_msg:null,
                        status_dtg:null
                  },
                  {
                        imp_uid:'imp_636581903313',
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-05-26 13:21:50',
                        order_price:158000,
                        order_cancel:0,
                        order_buyer_name:'김재윤',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'10.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'failed',
                        status_msg:'카드정보 입력 오류',
                        status_dtg:'2016-05-26 13:25:15'
                  },
                  {
                        imp_uid:'imp_636581903314',
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-05-26 13:27:21',
                        order_price:158000,
                        order_cancel:0,
                        order_buyer_name:'김재윤',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'PC',
                        client_os:'Windows',
                        client_os_v:'10.0.1',
                        client_browser:'Explorer',
                        client_browser_v:'11.0.0.1',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'failed',
                        status_msg:'카드사 모듈에서 에러',
                        status_dtg:'2016-05-26 13:32:14'
                  },
                  {
                        imp_uid:'imp_636581903315',
                        merchant_uid:'wc_order_573dbc46c04f4',
                        order_title:'컨센터블 멤버쉽',
                        order_dtg:'2016-05-26 13:40:21',
                        order_price:158000,
                        order_cancel:0,
                        order_buyer_name:'김재윤',
                        order_buyer_email:'sjkim@cloud4u.co.kr',
                        order_buyer_phone:'010-4518-5664',
                        order_buyer_address:'[121-912] 서울 마포구 성암로 330 A동 510호',
                        client_device:'Mobile',
                        client_os:'iOS',
                        client_os_v:'9.3.1',
                        client_browser:'Chrome',
                        client_browser_v:'50.0.2661.102',
                        pg_name:'이니시스 웹표준',
                        pg_storeid:'INIpayTest',
                        pg_pay_method:'신용카드',
                        pg_card_quota:'일시불',
                        status:'paid',
                        status_msg:"결제가 성공적으로 완료되었습니다.",
                        status_dtg:"2016-05-26 13:43:02"
                  }
            ]

            // 기본 변수
            vm.last = vm.orderList.length-1;
            vm.no_answer_limit = 300 // 최종 응답에서 300초(5분) 지날때까지 응답 업데이트가 없으면 ready여도 결제중단으로 봄

            // 최초 활성화
            vm.initial = function(){
                  for(var i=0; i<vm.orderList.length; i++){
                        vm.auto_push(i);      
                  }
            }

            // 간격
            function time_diff(impuid){
                  var start = moment(vm.orderList[impuid].order_dtg);
                  var end = moment(vm.orderList[impuid].status_dtg);
                  var over_wait_limit = ((moment()-start)/1000) >= vm.no_answer_limit;
                  return over_wait_limit;
            }

            // 응답없는 상태로 5분 지날 경우 결제상태 자동 업데이트
            vm.auto_push = function(impuid){
                  if(vm.orderList[impuid].status == "ready" && time_diff(impuid) == true){
                        var time = moment(vm.orderList[impuid].order_dtg).unix() + vm.no_answer_limit;
                        var time2 = moment.unix(time).format("YYYY-MM-DD HH:mm:ss");
                        vm.orderList[impuid].status_dtg = time2;
                        vm.orderList[impuid].status_msg = "결제 진행중 사용자 응답없음"
                  }
            }

            // 최종상태 한글변환
            vm.status = function(impuid){
                  var status = vm.orderList[impuid].status;
                  if(status == "ready" && time_diff(impuid) == false) return "결제 진행중";
                  else if(status == "paid") return "결제성공";
                  else if(status == "failed" || time_diff(impuid) == true) return "결제실패";
                  else if(status == "canceled") return "결제취소";
                  else return "정보없음";
            }

            // 최종상태 색상변환
            vm.status_to_color = function(status){
                  if(status == "ready") return "warn";
                  else if(status == "failed") return "danger";
                  else if(status == "paid") return "success";
                  else if(status == "canceled") return "info";
                  else if(status == "wait") return "bg-sm";
                  else return "white";
            }

            // 상태메시지
            vm.status_msg = function(impuid){
                  if (vm.orderList[impuid].status_dtg == null) return "-----";
                  return vm.orderList[impuid].status_msg
            }

            // 발생일시
            vm.status_dtg = function(impuid){
                  if (vm.orderList[impuid].status_dtg == null) return "-----";
                  return vm.orderList[impuid].status_dtg
            }

            // 시도차수 : 이전에 몇개의 IMPUID가 있는지?
            vm.trycount = function(impuid){
                  var cnt = 0;
                  for(var i=0; i<vm.orderList.length; i++){
                        if(vm.orderList[impuid].imp_uid == vm.orderList[i].imp_uid) var cnt = i+1;
                  }
                  return cnt + "차 시도";
            }

            // 결제 소요시간 카운트 (개별)
            vm.timecount = function(impuid){
                  var start = moment(vm.orderList[impuid].order_dtg);
                  var end = moment(vm.orderList[impuid].status_dtg);
                  if(vm.orderList[impuid].status_dtg == null) var end = moment();
                  var diff = moment.preciseDiff(end,start);
                  return diff;
            }

            // 결제 소요시간 카운트 (개별)
            vm.timecount_mid = function(index){
                  var start = moment(vm.midList[index].order_dtg);
                  var end = moment(vm.midList[index].status_dtg);
                  if(vm.midList[index].status_dtg == null) var end = moment();
                  var diff = moment.preciseDiff(end,start);
                  return diff;
            }

            // 결제 소요시간 카운트 (전체)
            vm.timecount_total = function(){
                  var start = moment(vm.orderList[0].order_dtg);
                  var end = moment(vm.orderList[vm.last].status_dtg);
                  if(vm.orderList[vm.last].status_dtg == null) var end = moment();
                  var diff = moment.preciseDiff(end,start);
                  return diff;
            }

            // 숫자 왼쪽에 0 붙이기
            function fillzero(obj, len) {
                  obj= '000000000000'+obj;
                  return obj.substring(obj.length-len);
            }

            // 결제 소요시간 카운트 (전체)
            vm.timecount_total_second = function(){
                  var start = moment(vm.orderList[0].order_dtg).unix();
                  var end = moment(vm.orderList[vm.last].status_dtg).unix();
                  if(vm.orderList[vm.last].status_dtg == null) var end = moment().unix();
                  var diff = end - start;
                  var total_seconds = diff - (diff%1)
                  var real_seconds = total_seconds % 60;
                  var total_minutes = ((total_seconds - real_seconds) / 60);
                  var real_minutes = total_minutes % 60;
                  var total_hours = ((total_minutes - real_minutes) / 24);
                  var fn_seconds = fillzero(real_seconds,2);
                  var fn_minutes = fillzero(real_minutes,2);
                  var fn_hours = fillzero(total_hours,2);
                  return fn_hours + ":" + fn_minutes + ":" + fn_seconds
            }


            // IMPUID 목록 클릭 시
            vm.imp_click = function(index){
                  vm.impuid_active = index;
            }

            // 우상단 방향키 클릭시 (prev)
            vm.prev_impuid = function(){
                  var no = vm.impuid_active;
                  if(no == 0) no = vm.last;
                  else if(no == 99) no = vm.last;
                  else no -= 1;
                  vm.impuid_active = no;
            }

            // 우상단 방향키 클릭시 (next)
            vm.next_impuid = function(){
                  var no = vm.impuid_active;
                  if(no == vm.last) no = 0;
                  else if(no == 99) no = 0;
                  else no += 1;
                  vm.impuid_active = no;
            }

            // 타임라인 가로길이 구하기
            vm.timeline_cell = function(ja_start,ja_end){

                  // 분모구하기
                  var mo_start_unix = moment(vm.orderList[0].order_dtg).unix();
                  if(vm.orderList[vm.last].status_dtg != null) var mo_last_unix = moment(vm.orderList[vm.last].status_dtg).unix();
                  else var mo_last_unix = moment(vm.orderList[vm.last].order_dtg).unix() + vm.no_answer_limit;
                  var denominator = mo_last_unix - mo_start_unix;

                  // 분자구하기
                  var ja_start_unix = moment(ja_start).unix();
                  if(ja_end != null) var ja_end_unix = moment(ja_end).unix();
                  else var ja_end_unix = ja_start_unix + vm.no_answer_limit;
                  var numerator = ja_end_unix - ja_start_unix;

                  // 가로길이
                  var percentage = numerator/denominator;
                  return percentage;

            }

            // 분모구하기
            vm.time_graph_denominator = function(){
                  var mo_start_unix = moment(vm.orderList[0].order_dtg).unix();
                  if(vm.orderList[vm.last].status_dtg == null) var mo_last_unix = moment(vm.orderList[vm.last].order_dtg).unix();
                  else var mo_last_unix = moment(vm.orderList[vm.last].status_dtg).unix();
                  var denominator = mo_last_unix - mo_start_unix;
                  return denominator;
            }

            // 분자구하기
            vm.time_graph_numerator = function(index){
                  var ja_start_unix = moment(vm.orderList[index].order_dtg).unix();
                  if(vm.orderList[index].status_dtg == null) var ja_last_unix = moment(vm.orderList[index].order_dtg).unix();
                  else var ja_last_unix = moment(vm.orderList[index].status_dtg).unix();
                  var numerator = ja_last_unix - ja_start_unix;
                  return numerator;
            }

            // 가로길이 퍼센테이지 반환하기
            vm.time_graph_percentage = function(index){
                  var numerator = vm.time_graph_numerator(index);
                  var denominator = vm.time_graph_denominator();
                  var percentage = numerator/denominator*100
                  return percentage;
            }

            // 분자구하기 (결제 사이의 대기구간)
            vm.time_graph_numerator_wait = function(index){
                  var ja_start_unix = moment(vm.orderList[index].status_dtg).unix();
                  if(vm.orderList[index+1].status_dtg == null) var ja_last_unix = ja_start_unix
                  else var ja_last_unix = moment(vm.orderList[index+1].order_dtg).unix();
                  var numerator = ja_last_unix - ja_start_unix;
                  return numerator;
            }

            // 가로길이 퍼센테이지 반환하기 (결제 사이의 대기구간)
            vm.time_graph_percentage_wait = function(index){
                  var numerator = vm.time_graph_numerator_wait(index);
                  var denominator = vm.time_graph_denominator();
                  var percentage = numerator/denominator*100
                  return percentage;
            }

            // 비교데이터 표 생성
            vm.spec_compare_header = function(){
                  if(vm.compare == 0) return "주문일시";
                  else if(vm.compare == 1) return "결제상태";
                  else if(vm.compare == 2) return "소요시간";
                  else if(vm.compare == 3) return "기록상태";
                  else if(vm.compare == 4) return "결제기기";
                  else if(vm.compare == 5) return "운영체제";
                  else if(vm.compare == 6) return "브라우저";
                  else if(vm.compare == 7) return "결제PG";
                  else if(vm.compare == 8) return "결제수단";
            }

            vm.spec_compare_next = function(){
                  if(vm.compare == 8) vm.compare = 0;
                  else vm.compare += 1;
            }

        
    }
})();
