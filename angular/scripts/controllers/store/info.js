// code style: https://github.com/johnpapa/angular-styleguide 

(function() {
    'use strict';
    angular
    .module('app')
    .controller('InfoCtrl', infosetting);

    function infosetting($scope){
        var vm = $scope;

        vm.is_admin = 1;

        vm.click_admin = function(){
            vm.is_admin = 1;
            $("#storeinfo .form-control").prop("disabled",false);
            $(".imgCropbox .box").removeClass("bg-xs");
            $("#save").removeClass("dp-none");
        }
        vm.click_member = function(){
            vm.is_admin = 2;
            $("#storeinfo .form-control").prop("disabled",true);
            $(".imgCropbox .box").addClass("bg-xs");
            $("#save").addClass("dp-none");
        }

    };
})();

