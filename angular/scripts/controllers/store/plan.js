// code style: https://github.com/johnpapa/angular-styleguide 

(function() {
    'use strict';
    angular
    .module('app')
    .controller('PlanCtrl', plansetting);

    function plansetting($scope, $interval){
        var vm = $scope;

        // 요금제 화면
        vm.priceList = [
            {
                name:"통계분석",
                icon:"presentation-7",
                content:"아임포트는 결제과정에서 생성된 데이터들을 분석하여 다양한 통계를 제공합니다. 고객들의 결제 경험을 개선하고, 데이터에 기반한 합리적인 의사결정을 내리는데 아임포트의 통계분석 기능을 활용해보세요.",
                price:19900,
                tag:"19,900원",
                unit:"/월",
                interval:"월정기"
            },{
                name:"정기결제 연동",
                icon:"calendar-3",
                content:"정기결제 연동에 필요한 암호화된 결제토큰을 별도의 보안서버 구축과정 없이도 안전하게 관리할 수 있습니다. 아임포트는 모든 구간에 SSL과 PKI 암호화 기술을 적용하여 안전한 결제환경을 제공합니다.",
                price:9900,
                tag:"9,900원",
                unit:"/월",
                interval:"월정기"
            },{
                name:"복수 PG 사용",
                icon:"diagram-1",
                content:"여러 PG를 동시에 이용하면 보다 편리하고 안정적인 결제환경을 만들 수 있습니다. 간편결제를 추가로 지원하거나 결제 트래픽을 분산 처리하는 등 복잡하고 다양한 결제의 니즈를 복수PG를 이용해 해결하세요.",
                price:55000,
                tag:"55,000원",
                unit:"",
                interval:"최초 1회"
            },{
                name:"API Call Limit 해제",
                icon:"graph-1",
                content:"아임포트는 월 10만건 이내는 무료이며, 그 이상의 API Call이 발생할 경우 Call당 과금됩니다. 10만건을 초과하면 자동으로 활성화되며, 갑자기 결제가 중단되지 않도록 월말에 정산하고 익월에 후불 청구됩니다.",
                price:10,
                tag:"1원",
                unit:"/1Call",
                interval:"월정기 (후불)"
            },{
                name:"통계분석 제외구간 복원",
                icon:"video-edition",
                content:"아임포트 통계분석은 요금제를 유지한 기간의 데이터를 대상으로 동작하기 때문에 도중에 해지한 뒤 재가입하시는 경우 해당 기간만큼의 공백이 발생합니다. 이 기능을 이용해 빈 구간의 데이터를 복원하실 수 있습니다.",
                price:99000,
                tag:"99,000원",
                unit:"/건",
                interval:"신청 시"
            }
        ]





    };

})();

