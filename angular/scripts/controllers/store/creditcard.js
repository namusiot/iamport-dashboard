(function() {
    'use strict';
    angular
    .module('app')
    .controller('CreditCardCtrl', creditcardsetting);

    function creditcardsetting($scope){
        var vm = $scope;

        // DB에 저장된 설정값
        vm.cardValue = [
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1},
            {active:null, month:1}
        ];

        // 카드사별 콘텐츠
        vm.cardList = [
            {name:"외환", image:"keb"},
            {name:"롯데", image:"lotte"},
            {name:"현대", image:"hyundai"},
            {name:"KB국민", image:"kb"},
            {name:"BC", image:"bc"},
            {name:"삼성", image:"samsung"},
            {name:"신한", image:"shinhan"},
            {name:"하나", image:"hana"},
            {name:"NH농협", image:"nh"},
            {name:"씨티", image:"citi"},
            {name:"우리", image:"woori"},
            {name:"해외비자", image:"visa"},
            {name:"해외마스터", image:"master"},
            {name:"해외JCB", image:"jcb"},
            {name:"해외아멕스", image:"amex"},
            {name:"해외다이너스", image:"diners"}
        ]

        // 카드 전체설정 기본값
        vm.toggle = true;
        vm.number_all = 1;

        // 개별카드 활성화가 null이면 자동 true로 설정
        vm.initial = function(){
            for(var i=0; i<vm.cardValue.length; i++){
                if(vm.cardValue[i].active == null) vm.cardValue[i].active = true;
            }
        }

        // 전체 체크박스 버튼 클릭할 때
        vm.toggle_all = function(){
            if(vm.toggle == false) vm.toggle = true;
            else if(vm.toggle == true) vm.toggle = false;
            for (var i=0; i<vm.cardValue.length; i++){
                vm.cardValue[i].active = vm.toggle;
            }
        }

        // 전체 증가 버튼 클릭할 때
        vm.number_up_all = function(){
            for(var i=0; i<vm.cardValue.length; i++) vm.cardValue[i].month += 1;
            vm.number_all += 1;
        }

        // 전체 감소 버튼 클릭할 때
        vm.number_down_all = function(){
            for(var i=0; i<vm.cardValue.length; i++){
                if(vm.cardValue[i].month == 1) vm.cardValue[i].month = 1;
                else vm.cardValue[i].month -= 1;
            }
            if(vm.number_all == 1) vm.number_all =1;
            else vm.number_all -= 1;
        }

        // 증가 버튼 누를 때
        vm.number_up = function(index){
            vm.cardValue[index].month += 1;
        }

        // 감소 버튼 누를 때
        vm.number_down = function(index){
            if(vm.cardValue[index].month == 1) vm.cardValue[index].month = 1;
            else vm.cardValue[index].month -= 1;
        }
        
        // 저장하기 버튼 누를 때 메시지
        vm.saving = function(condition){
            var cond = condition;
            vm.messageshow(cond,3000);
        }

        // 메시지 생성 (조건, 아이콘, 메시지내용, 색상)
        vm.message = function(condition,icon,text,color){
            var dom = '<div class="alert alert-'+color+' '+condition+' dk ls-sm _500 p-y-sm text-sm dp-none"><i class="icon icon-'+icon+' pull-left m-r-xs"></i>'+text+'</div>'
            return dom;
        }

        // 메시지 보여주기
        vm.messageshow = function(condition,time){
            $(".alert."+condition).addClass("dp-block");
            $(".alert."+condition).addClass("animated");
            $(".alert."+condition).addClass("fadeIn");
            $(".alert."+condition).removeClass("fadeOut");
            setTimeout(function(){ $(".alert."+condition).addClass("fadeOut"); },time); 
            setTimeout(function(){ $(".alert."+condition).removeClass("dp-block"); },time+(time*0.1)); 
            setTimeout(function(){ $(".alert."+condition).addClass("dp-none"); },time+(time*0.1));
        }
    }
})();
