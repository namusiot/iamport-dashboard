// code style: https://github.com/johnpapa/angular-styleguide 

(function() {
    'use strict';
    angular
    .module('app')
    .controller('PGsetting2Ctrl', PGsetting);

    function PGsetting($scope, $interval){
        var vm = $scope;

        // moment.js 한글
        moment.locale('ko');

        // DB에 저장되어 있는 가맹점의 설정정보
        vm.start_free = null; // 미결제 상태에서 PG 처음 설정
        vm.start_test = null; // 무료체험 활성화했을 때
        vm.start_paid = null; // 유료결제 했을 때

        // DB에 저장되어 있는 가맹점의 DB정보
        vm.savedPGList = [
            {
                pgno:null, // 선택한 PG번호
                since:null, // (가장 최근) PG를 설정한 날짜
                testmode:null, // 테스트모드 true/false
                revenue:null, // 매출금액
                request:null, // 결제건수
                pginfo1:null, // 이하 PG설정값 (PG사 공용, 덮어쓰기됨)
                pginfo2:null,
                pginfo3:null,
                pginfo4:null,
                pginfo5:null,
                pginfo6:null,
                pginfo7:null,
                pginfo8:null,
                pginfo9:null
            } // 복수PG인 경우 object 증가
        ];

        // PG별 콘텐츠
        vm.pgList = [
            {
                name:"나이스정보통신",
                image:"nice",
                promotion:"아임포트는 나이스정보통신과 제휴하여 신용카드 3.2%, 실시간 계좌이체 1.8%, 가상계좌 300원/건, 최초가입비 20만원, 연관리비 평생면제의 혜택을 드리는 프로모션을 진행하고 있습니다.",
                testmode_on : "나이스정보통신에서 제공하는 테스트계정으로 결제가 진행됩니다. 실제로 결제승인이 이루어지지만, 매일 자정이 되기 전에 자동으로 취소됩니다. 테스트모드를 OFF하시면 PG관련 실제 정보를 입력하실 수 있는 필드가 나타납니다.",
                testmode_off_do : "계약시 발급된 상점아이디(MID)와 KEY, 결제취소 비밀번호를 입력해주세요.",
                testmode_off_tip : "KEY는 나이스정보통신 관리자 페이지의 \"가맹점정보 > KEY관리\"에서, 취소비밀번호는 \"가맹점정보 > 기본정보\" 페이지 중간에서 확인하실 수 있습니다.",
                input_count : 3
            },{
                name:"JTNET",
                image:"jtnet",
                promotion:"아임포트는 JTNET과 제휴하여 신용카드 3.2%, 실시간 계좌이체 1.8%, 가상계좌 300원/건, 최초가입비 20만원, 연관리비 평생면제의 혜택을 드리는 프로모션을 진행하고 있습니다.",
                testmode_on : "JTNet에서 제공하는 테스트계정으로 결제가 진행됩니다. 실제로 결제승인이 이루어지지만, 매일 자정이 되기 전에 자동으로 취소됩니다. 테스트모드를 OFF하시면 PG관련 실제 정보를 입력하실 수 있는 필드가 나타납니다.",
                testmode_off_do : "계약시 발급된 상점아이디(MID)와 KEY, 결제취소 비밀번호를 입력해주세요.",
                testmode_off_tip : "KEY는 tPay 관리자 페이지에서 \"회원사정보 > KEY관리\"에서, 취소비밀번호는 \"회원사정보 > 일반정보\" 페이지 중간에서 확인하실 수 있습니다.",
                input_count : 3
            }
        ];

        // PG 편집모드용 빈값
        vm.replacePGList = {
            pgno:null,
            since:null,
            testmode:null,
            revenue:null,
            request:null,
            pginfo1:null, pginfo2:null, pginfo3:null, pginfo4:null, pginfo5:null, pginfo6:null, pginfo7:null, pginfo8:null, pginfo9:null
        }

        // 최초 설정
        $interval(function(){}, 1000); // 동적인 숫자카운트 효과
        vm.initial = function(){
            if(vm.pg_focus == null) vm.pg_focus = 0; // PG슬롯 선택 안했으면 1번 슬롯 자동 선택
            set_step();
            set_dayleft();
            set_slot();
        }

        // 현재 상태 : 날짜 기록 유무로 판단
        function set_step(){
            if(vm.start_free == null || vm.start_test == null) vm.step = "free";
            else if(vm.start_paid == null) vm.step = "test";
            else vm.step = "paid"
        }

        // 무료체험 기간 남은 날짜 계산
        function set_dayleft(){
            var endpoint = moment(vm.start_test).add(14,'days').endOf('day');
            vm.dayleft = (diff_day(endpoint)-1)*-1;
        }

        // free와 무료체험기간 끝났을 때 슬롯 1개만 활성화
        function set_slot(){
            if(vm.step == "free" || (vm.step=="test" && vm.dayleft < 0)){
                for(var i=1; i<vm.savedPGList.length; i++){
                    vm.savedPGList.splice(i,1);
                    vm.pg_focus = 0
                }
            }
        }

        // 숫자표기 : 1) 3자리쉼표 2) 1억이 넘어가면 상위단위로 변경 3) 1이하이면 소수점표시 4) 음수면 에러 출력
        function no(number) {
            if(number>100000000) return (parseInt(number/100000000)).toLocaleString() + "억 " + parseInt((number%100000000)/10000).toLocaleString() + "만";
            else if(number>0&&number<1) return number.toFixed(2);
            else if(number<0) return "ERROR";
            else return parseInt(number).toLocaleString();
        }

        // 오늘부터 며칠전인지 계산해서 일수로 반환
        function diff_day(since){
            var m1 = moment().format('YYYY-MM-DD');
            var m2 = moment(since).format('YYYY-MM-DD');
            var diff = moment().diff(m2,m1);
            var diffday = (diff/1000/60/60/24).toFixed(0);
            return diffday;
        }

        // 당일일 경우에 사용하는 몇시간 남았는지 카운트다운
        function diff_time(since){
            var m1 = moment().format("YYYY-MM-DD HH:mm:ss");
            var m2 = moment(since).format("YYYY-MM-DD HH:mm:ss");
            var diff = moment.preciseDiff(m2,m1);
            return diff;
        }

        // 무료체험 활성화 모달에서 OK할 때
        vm.starttest = function(){
            vm.step = 'test';
            vm.start_test = moment().format("YYYY-MM-DD HH:mm:ss");
            if(vm.start_free==null) vm.start_free = moment().format("YYYY-MM-DD HH:mm:ss");
            vm.savedPGList.push({pgno:null});
            set_dayleft();
        }

        // (선택PG 정보요약) PG 최초 설정일
        vm.SavedStartDate = function(since){
            if(since==null) return "-----";
            else return moment(since).format('YYYY-MM-DD');
        }

        // (선택PG 정보요약) 사용기간 자동계산 표시
        vm.SavedUsingDate = function(since){
            var m1 = moment().format('YYYY-MM-DD');
            var m2 = moment(since).format('YYYY-MM-DD');
            var diff = moment.preciseDiff(m1, m2);
            if(since==null) return "-----";
            else if(diff == "") return "0일";
            else return diff;
        }

        // (선택PG 정보요약) 테스트모드 ON/OFF 표시
        vm.SavedTestmode = function(testmode,since){
            if(testmode==null||since==null) return "-----";
            else if(testmode==true) return "ON";
            else return "OFF";
        }

        // (선택PG 정보요약) 누적거래금액
        vm.SavedRevenue = function(revenue,since){
            if(revenue==null||since==null) return "-----";
            else return no(revenue) + "원";
        }

        // (선택PG 정보요약) 누적결제건수
        vm.SavedRequest = function(request,since){
            if(request==null||since==null) return "-----";
            else return no(request) + "건";
        }

        // (선택PG 정보요약) 일평균거래금액
        vm.SavedAVGRevenue = function(revenue,since){
            if(revenue==null||since==null) return "-----";
            else {
                var diffday = diff_day(since);
                var avgrevenue = parseInt(revenue / diffday);
                if(diffday==0) return no(revenue) + "원";
                else return no(avgrevenue) + "원";
            }
        }

        // (선택PG 정보요약) 일평균결제건수
        vm.SavedAVGRequest = function(request,since){
            if(request==null||since==null) return "-----";
            else {
                var diffday = diff_day(since);
                var avgrequest = request / diffday;
                if(diffday==0) return no(request) + "건";
                else return no(avgrequest) + "건";
            }
        }

        // PG 삭제 버튼 클릭 시
        vm.deleteslot = function(){
            if(vm.savedPGList.length>1){
                vm.savedPGList.splice(vm.pg_focus,1);
                if(vm.pg_focus<=0) vm.pg_focus = 0
                else vm.pg_focus = vm.pg_focus-1;
            }else {
                vm.savedPGList[vm.pg_focus].pgno = null;
                vm.savedPGList[vm.pg_focus].since = null;
            }
        }

        // PG 추가 버튼 클릭 시
        vm.addslot = function(){
            vm.emptyslot = 0;
            for(var i=0; i<vm.savedPGList.length; i++){
                if(vm.savedPGList[i].pgno == null){
                    vm.emptyslot += 1;
                }
            }
            if(vm.emptyslot < 1){
                vm.savedPGList.push({pgno:null});
            }else {
                $('#add-slot-error-modal').modal();
            }
        }

        // PG편집모드 진입
        function enter_edit_mode(){
            $('#pg_using').addClass("dp-none");
            $('#step_free').addClass("dp-none");
            $('#step_test').addClass("dp-none");
            $('#pg_modify').removeClass("dp-none");
            $('#pg_select').removeClass("dp-none");
            $('#pg_testmode').removeClass("dp-none");
            $('#pg_save').removeClass("dp-none");
            $('.pg_form').removeClass("dp-none");
        }

        // PG편집모드 종료
        function exit_edit_mode(){
            $('#pg_using').removeClass("dp-none");
            $('#step_free').removeClass("dp-none");
            $('#step_test').removeClass("dp-none");
            $('#pg_modify').addClass("dp-none");
            $('#pg_select').addClass("dp-none");
            $('#pg_testmode').addClass("dp-none");
            $('#pg_save').addClass("dp-none");
            $('.pg_form').addClass("dp-none");
        }

        // PG편집모드 진입 시 기존 Data 있으면 로드, 없으면 빈칸
        vm.edit_slot = function(check,object){
            enter_edit_mode();
            angular.copy(object,vm.replacePGList);
            if(vm.replacePGList.pgno==null){
                vm.replacePGList.testmode = true;
                $("#pg_testmode").addClass("dp-none");
            }
            // $.extend(vm.replacePGList, object);
        }

        // object에서 value만 분리하기
        Object.values = function(object) {
            var values = [];
            for(var property in object) {
                values.push(object[property]);
            }
            return values;
        }

        // 테스트모드 아닐 때 입력폼 비어있는지 여부 체크
        function checking(source,target){
            var check = 1;
            for(var i=5;i<5+vm.pgList[vm.replacePGList.pgno].input_count;i++){
                if(Object.values(vm.replacePGList)[i] == null || Object.values(vm.replacePGList)[i] == "") check = check * 0;
            };
            if(check==0) return true;
            else return false;
        }

        // (PG 편집모드) 특정 PG 선택 시
        vm.pgclick = function (thispg,thisslot,source,target) {
            vm.replacePGList.pgno = thispg;
            if(source.pgno!=target.pgno){
                target.testmode = true;
                target.pginfo1 = null;
                target.pginfo2 = null;
                target.pginfo3 = null;
                target.pginfo4 = null;
                target.pginfo5 = null;
                target.pginfo6 = null;
                target.pginfo7 = null;
                target.pginfo8 = null;
                target.pginfo9 = null;
            }else{
                target.testmode = source.testmode;
                target.pginfo1 = source.pginfo1;
                target.pginfo2 = source.pginfo2;
                target.pginfo3 = source.pginfo3;
                target.pginfo4 = source.pginfo4;
                target.pginfo5 = source.pginfo5;
                target.pginfo6 = source.pginfo6;
                target.pginfo7 = source.pginfo7;
                target.pginfo8 = source.pginfo8;
                target.pginfo9 = source.pginfo9;
            }
            $("#pg_testmode").removeClass("dp-none");
        }

        // (PG 편집모드) 취소 버튼 클릭 시
        vm.edit_cancel = function(){
            exit_edit_mode();
        }

        // (PG 편집모드) PG정보 저장할 때 로직
        vm.edit_save_ok = function(source,target){
            angular.copy(source,target);
            target.since = new Date();
            target.revenue = 0;
            target.request = 0;
            exit_edit_mode();
        }

        // (PG 편집모드) 저장 버튼 클릭 시
        vm.edit_save = function(source,target){
            var cond = null;
            var check_result = checking(source,target);
            if(vm.replacePGList.testmode == false && check_result==true){
                var cond='save_empty';
                vm.messageshow(cond,3000); // 저장버튼 위에 빈칸 채워달라 에러메시지 표시
            }else {
                if(target.pgno!=source.pgno && target.pgno!=null) $('#change-pg-modal').modal();
                else vm.edit_save_ok(source,target);
            }
        }

        // 무료체험 광고의 PG아이콘 바뀌는 애니메이션용 초(second) 카운트
        vm.change_pg_icon = function(number){
            var number_change = (moment().second()+number)%7;
            return number_change;
        }

        // 무료체험 카운터의 큰 글씨
        vm.dayleft_main = function(){
            if(vm.count_days>0) return vm.count_days + "일 남았습니다.";
            else if(vm.count_days<0) return "무료체험이 종료되었습니다.";
            else return "무료체험이 곧 종료됩니다.";
        }

        // 무료체험 카운터의 작은 글씨
        vm.dayleft_sub = function(){
            var endpoint = moment(vm.start_test).add(14,'days').endOf('day');
            var timeleft = diff_time(endpoint);
            if(vm.dayleft>0) return "무료체험이 " + endpoint.format("YYYY년 M월 D일") + " 자정에 종료됩니다.";
            else if(vm.dayleft<0) return "시작일시 ("+ vm.start_test + " ~ " + endpoint.format("YYYY-MM-DD HH:mm:ss") +") 기존에 저장된 추가PG 설정정보가 초기화되었습니다. 복수PG 기능이 다시 필요하신 경우 언제든지 아래의 결제를 완료한 뒤 사용하실 수 있습니다.";
            else return "무료체험이 오늘("+ endpoint.format("YYYY-MM-DD") +") 자정에 종료됩니다."
        }

        // (무료체험 타이머 이미지) 숫자 왼쪽에 0 채우기
        var zero_number = function(source, num){
            var zero = '';
            source = source.toString();
            if (source.length < num) {
            for (var i=0; i<num-source.length; i++)
                zero += '0';
            }
            return zero + source;
        }

        // 무료체험 타이머 이미지 구현
        vm.timer_number = function(){
            var endpoint = moment(vm.start_test).add(14,'days').endOf('day');
            var m1 = moment().format("YYYY-MM-DD HH:mm:ss");
            var m2 = moment(endpoint).format("YYYY-MM-DD HH:mm:ss");
            var diff = moment().diff(m2,m1);
            var totalsec = parseInt((diff-1000)/1000)*-1;
            var totalmin = totalsec/60;
            var totalhour = totalmin/60;
            var totalday = totalhour/24;
            if(totalsec<0){
                var totalsec = 0;
                var totalmin = 0;
                var totalhour = 0;
                var totalday = 0;
            }
            vm.count_days = zero_number(parseInt(totalday),2);
            vm.count_hours = zero_number(parseInt(totalhour%24),2);
            vm.count_mins = zero_number(parseInt(totalmin%60),2);
            vm.count_secs = zero_number(parseInt(totalsec%60),2);
        }

        // 메시지 생성 (조건, 아이콘, 메시지내용, 색상)
        vm.message = function(condition,icon,text,color){
            var dom = '<div class="alert alert-'+color+' '+condition+' dk ls-sm _500 p-y-sm text-sm dp-none"><i class="icon icon-'+icon+' pull-left m-r-xs"></i>'+text+'</div>'
            return dom;
        }

        // 메시지 보여주기
        vm.messageshow = function(condition,time){
            $(".alert."+condition).addClass("dp-block");
            $(".alert."+condition).addClass("animated");
            $(".alert."+condition).addClass("fadeIn");
            $(".alert."+condition).removeClass("fadeOut");
            setTimeout(function(){ $(".alert."+condition).addClass("fadeOut"); },time); 
            setTimeout(function(){ $(".alert."+condition).removeClass("dp-block"); },time+(time*0.1)); 
            setTimeout(function(){ $(".alert."+condition).addClass("dp-none"); },time+(time*0.1));
        }

    };

})();

