// code style: https://github.com/johnpapa/angular-styleguide 

(function() {
    'use strict';
    angular
    .module('app')
    .controller('WalletCtrl', walletsetting);

    function walletsetting($scope, $interval){
        var vm = $scope;

        vm.regcardList = [
        	{
        		no:0,
        		name:"마스터",
        		logo:"master",
        		lastno:"6925",
        		regdate:"2016-01-15",
        		main:false,
        		lock:false
        	},{
        		no:1,
        		name:"현대",
        		logo:"hyundai",
        		lastno:"3425",
        		regdate:"2016-01-25",
        		main:true,
        		lock:false
        	},{
        		no:2,
        		name:"롯데",
        		logo:"lotte",
        		lastno:"6925",
        		regdate:"2016-02-13",
        		main:false,
        		lock:true
        	},{
        		no:3,
        		name:"비자",
        		logo:"visa",
        		lastno:"5532",
        		regdate:"2016-04-03",
        		main:false,
        		lock:true
        	}
        ]

        // 대표카드 활성화
        function maincard(){
        	var i = vm.regcardList.length;
        	while( i-- ) {
        		if( vm.regcardList[i].main == true ) break;
        	}
        	vm.card_focus = vm.regcardList[i].name;
        	vm.card_focus_index = i;
        	// alert(vm.card_focus_index);
        }
        maincard();

        // 등록된 카드 삭제
        vm.deletecard = function(index){
			var i = vm.regcardList.length;
			while( i-- ) {
			    if( vm.regcardList[i].no == index ) break;
			}
			if(vm.regcardList[i].lock==false){
        		vm.regcardList.splice(i,1);
        	}else {
        		alert('이미 결제중이다. 삭제불가')
        		vm.noclick = vm.card_focus_index;
        	}
        }

        // 카드 선택
        vm.cardclick = function(index){
        	var i = vm.regcardList.length;
	    	while( i-- ) {
	    		vm.regcardList[i].active = false;
	    	}
	    	vm.regcardList[index].active = true;
	    	vm.card_focus_index = index;
	    	vm.card_focus = vm.regcardList[vm.card_focus_index].name;
        	// alert(index)
        	// if(index == vm.noclick){
        		
        	// }else {
        	// 	alert('카드변경 실행');
        	// 	// if(vm.regcardList[vm.card_focus].lock==false){
		        	
	        // 	// }else {
	        // 	// 	alert('잠겨있다.')
	        // 	// }	
        	// }
        }


    };

})();
